// Why you should use stream
// for example in this code

const fs = require('fs');

// For Large File

const readStream = fs.createReadStream('./lagefile.txt', 'utf8');
readStream.on('data', (chunk) => {
    console.log(chunk);
});

// For small FIle
//
// fs.readFile('./largeFile.txt', (err, file) => {
//     if (err) {
//         console.log(err);
//     }else{
//         console.log(file);
//     }
// })