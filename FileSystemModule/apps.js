// File System Module part 2
const fs = require('fs');

// Created A Directory and Create a File
fs.mkdir('example', (err) => {
    if (err) {
        console.log(err);
    }else{
        fs.writeFile('./example/document.txt', 'Document Text', (err) =>{
            if (err) {
                console.log(err);
            }else{
                console.log('Successfully created Directory and File');
            }
        })
    }
});

// Deleted A Directory and deleted a File
fs.unlink('./example/document.txt', (err) => {
    if (err) {
        console.log(err);
    }else{
        fs.rmdir('example', (err) => {
            if (err) {
                console.log(err);
            }else{
                console.log('deleted Directory successfully')
            }
        });
    }
});

// Delete A file in folder
fs.readdir('example', (err, files) => {
    if (err) {
        console.log(err);
    }else{
        for (let file of files) {
            fs.unlink('./example/' + file, (err) => {
                if (err) {
                    console.log(err);
                }else{
                    console.log('successfully deleted files');
                }
            });
        }
    }
});

// Make A Directory
fs.mkdir('example', (err) => {
    if (err) {
        console.log(err);
    }else{
        console.log('Folder successfully created');
    }
});

// Delete A Directory
fs.rmdir('example', (err) => {
    if (err) {
        console.log(err);
    }else{
        console.log('Folder deleted created');
    }
});