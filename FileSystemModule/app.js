const app = require('fs');
// Create A File
fs.writeFile('example.txt', "This is an Example", (err) => {
    if (err) {
        console.log(err);
    }else{
        console.log('file successfully created !');
        fs.readFile('example.txt','utf8', (err, file) => {
            if (err) {
                console.log(err);
            }else{
                console.log(file);
            }
        });
    }
});

// Edit File Name
fs.rename('example.txt', 'example2.txt', (err) =>{
    if (err) {
        console.log(err);
    }else{
        console.log('Successfully renamed the file');
    }
});

// Append Data File
fs.appendFile('example2.txt','Some data being appended', (err) =>{
    if (err) {
        console.log(err);
    }else{
        console.log('Successfully appended the file');
    }
});

// Delete File
fs.unlink('example2.txt', (err) =>{
    if (err) {
        console.log(err);
    }else{
        console.log('Successfully deleted the file');
    }
});