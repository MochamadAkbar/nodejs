const EvenEmitter = require('events');
const evenEmitter = new EvenEmitter();

evenEmitter.on('tutorial', (num1, num2) =>{
    console.log(num1 + num2);
});

evenEmitter.emit('tutorial', 1, 2);

class Person extends EvenEmitter{
    constructor(name){
        super();
        this._name = name;
    }

    get name(){
        return this._name;
    }
}

let Akbar = new Person('Akbar');
let Diki = new Person('Diki');

Diki.on('name', () => {
    console.log('He name is ' + Diki.name)
});

Akbar.on('name', () => {
    console.log('My name is ' + Akbar.name);
});

Akbar.emit('name');
Diki.emit('name');