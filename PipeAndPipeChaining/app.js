const fs = require('fs');
const zlib = require('zlib');

const gzip = zlib.createGzip(); // Create a .gz
const readStream = fs.createReadStream('./example.txt', 'utf8');
const writeStream = fs.createWriteStream('./example2.txt.gz');

readStream.pipe(gzip).pipe(writeStream);


const gunzip = zlib.createGunzip(); // Unzip a .gz
const readStream = fs.createReadStream('./example2.txt.gz');
const writeStream = fs.createWriteStream('uncompressed.txt');

readStream.pipe(gunzip).pipe(writeStream);
// Pipe
// readStream.pipe(writeStream);

// Pipe Chaining
// readStream.pipe(gzip).pipe(writeStream);

// This is Wrong
// readStream.on('data', (chunk) => {
//     writeStream.write(chunk);
// })