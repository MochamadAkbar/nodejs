const fs = require('fs');
const readStream = fs.createReadStream('./example.txt', 'utf8');
const writeStream   = fs.createWriteStream('example2.txt');

// Read data
// readStream.on('data', (chunk) =>{
//     console.log(chunk);
// });

// Read data and copy data to new file
readStream.on('data', (chunk) =>{
    writeStream.write(chunk);
});