// Create Http Server

const http = require('http');
const server = http.createServer((req, res) => {
    // res.write('Hello World from Node Js ! ');
    // res.end();

    if(req.url === '/'){
        res.write('Hello World From Node Js');
        res.end();
    }else{
        res.write('Using some Other domain');
        res.end();
    }
});

server.listen('3000');